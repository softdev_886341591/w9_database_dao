/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdev.databasedao.service;

import softdev.databasedao.dao.UserDao;
import softdev.databasedao.model.User;

/**
 *
 * @author acer
 */
public class UserService {
    public User login(String name , String password){
        UserDao userDao = new UserDao();
        User user = userDao.getByName(name);
        if(user!=null && user.getPassword().equals(password)){
            return user;
        }
        return null;
    }
}
